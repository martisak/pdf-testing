DOCKER=docker
DOCKER_COMMAND=run --rm -w /data/ --env LATEXMK_OPTIONS_EXTRA=$(LATEXMK_OPTIONS_EXTRA)
DOCKER_MOUNT=-v`pwd`:/data

all: render

update:
	-apt-get update
	-apt-get install -y unzip

pdf: update
	-make -C example compile

clean:
	-make -C example clean

dist-clean:
	-make -C example dist-clean

pip:
	pip install -r requirements.txt

render:
	$(DOCKER) $(DOCKER_COMMAND) $(DOCKER_MOUNT) blang/latex:ctanfull \
		make pdf

check:
	pytest --durations=3 -vv --pdf example/Conference-LaTeX-template_10-17-19/conference_101719.pdf

test:
	$(DOCKER) $(DOCKER_COMMAND) $(DOCKER_MOUNT) python:3.8 \
		make pip check

debug:
	$(DOCKER) $(DOCKER_COMMAND) -it $(DOCKER_MOUNT) python:3.8 \
		bash
