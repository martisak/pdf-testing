![Pipeline status](https://gitlab.com/martisak/pdf-testing/badges/master/pipeline.svg)

# How to beat publisher checks with LaTeX document unit testing

Support files for blog post [How to beat publisher PDF checks with LaTeX document unit testing](https://blog.martisak.se/2020/05/16/latex-test-cases/).

Compile in Docker with `make render test` and locally with `make pdf check`. Note that at least one test case can fail due to inconsistent versions of pdfTeX.