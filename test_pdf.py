from pathlib import Path
import fitz
from intervaltree import IntervalTree, Interval
import pytest
from titlecase import titlecase
import yaml

"""
This module defines a set of test cases that mimic the behavior of EDAS
(https://edas.info/).

An example output from EDAS (without faults): > The paper has 6 pages, has a
paper size of 8.5x11 in (letter), is formatted in 2 columns, with a gutter of
0.201 inches (smallest on pg. 5), the most common font size is 9.96 pt, the
average line spacing is 11.95 pt, margins are 0.673 (L) x 0.653 (R) x 0.701 (T)
x 0.990 (B) inches, uses PDF version 1.7 and was created by TeX.
"""


def get_interval_width(interval, points_per_inch=72):
    """
    Helper function to get the width of an intervaltree.Interval
    in inches.
    """
    return (interval.end - interval.begin) / \
        points_per_inch


@pytest.fixture
def config():
    return yaml.load(open("config.yml"), Loader=yaml.FullLoader)


@pytest.fixture(scope="session")
def pdf_document(pdf):
    """
    This fixture opens the PDF document for reading,
    and closes the file when the fixture goes out of scope.
    """

    assert pdf
    pdf_document = fitz.open(pdf)

    yield pdf_document
    pdf_document.close()


def test_annotations(pdf_document):
    """
    Test that there are no annotations.
    """

    for page1 in pdf_document:
        annotations = list(page1.annots())
        assert not annotations


def test_required_text(pdf_document, config):
    """
    Test that each required text is found in the document.
    """

    for text in config.get("required_text", []):
        hits = 0
        for page1 in pdf_document:
            hits += len(page1.getTextPage().search(text))

        assert hits > 0


def test_title_case(pdf_document):
    """
    Test that the title (first block on first page)
    is title cased properly.
    """

    page1 = pdf_document.loadPage(0)
    title = page1.getTextBlocks()[0][4]
    assert title == titlecase(title)


def test_no_links(pdf_document):
    """
    Test that no links appear on any page.
    """

    for page1 in pdf_document:
        assert len(page1.getLinks()) == 0


def test_no_bookmarks(pdf_document):
    """
    Test that the document does not contain bookmarks
    """

    assert len(pdf_document.getToC()) == 0


def test_file_size(pdf, config):
    """
    Test that the filesize is below the limit.
    """

    assert Path(pdf).stat().st_size < config.get("max_file_size", 0)


def test_pages(pdf_document, config):
    """
    Test that the number of pages is between
    the minimum and the maximum number of pages.
    """

    assert config.get("min_pages", 1) <= \
        pdf_document.pageCount <= \
        config.get("max_pages", 5)


def test_pdf_version(pdf_document, config):
    """
    Test that the PDF version is at least the specified
    """

    version = float(pdf_document.metadata.get("format", None).split(" ")[1])

    assert version >= config.get("min_version", 1.4)


def test_metadata(pdf_document, config):
    """
    For each of the specified fields, check that the result
    is as expected.
    """

    metadata_fields = ["author", "creator", "title",
                       "subject", "keywords", "producer",
                       "encryption"]

    for field in metadata_fields:
        assert pdf_document.metadata.get(
            field, None) == config["metadata"].get(field, None)


def test_embedded_fonts(pdf_document):
    """
    Check that all fonts are extractable. This will at least loop through
    the embedded fonts and check their types.
    """

    for page in pdf_document:
        for f in page.getFontList():
            _, ext, fonttype, _ = pdf_document.extractFont(f[0])
            assert fonttype in ["TrueType", "Type1"]
            assert ext != "n/a"


def test_dimensions(pdf_document, config):
    """
    This test case loops through pages and checks
        - paper size
        - gutter width
        - number of columns

    Finally it saves a document with the found columns and bounding boxes
    overlayed.
    """

    blue = (0, 0, 1)

    count = 0
    for page1 in pdf_document:

        full_tree_x = IntervalTree()
        full_tree_y = IntervalTree()
        tree_x = IntervalTree()
        tree_y = IntervalTree()

        blks = page1.getTextBlocks()  # Read text blocks of input page
        img = page1.newShape()  # Prepare contents object

        # Calculate CropBox & displacement
        disp = fitz.Rect(page1.CropBoxPosition, page1.CropBoxPosition)

        croprect = page1.rect + disp
        full_tree_x.add(Interval(croprect[0], croprect[2]))
        full_tree_y.add(Interval(croprect[1], croprect[3]))

        # This tests paper size
        assert list(croprect) == config["pages"].get("papersize")

        for b in blks:  # loop through the blocks
            r = fitz.Rect(b[:4])  # block rectangle

            # add dislacement of original /CropBox
            r += disp
            x0, y0, x1, y1 = r

            # Dangerous!
            if count > config.get("skip_boxes_on_first_page", 2):
                tree_x.add(Interval(x0, x1))
                tree_y.add(Interval(y0, y1))

            count += 1

        tree_x.merge_overlaps()
        tree_y.merge_overlaps()

        # Must be two columns
        assert len(tree_x) == 2

        for intrv in tree_x:

            a = [intrv[0], tree_y.begin(), intrv[1], tree_y.end()]

            re = fitz.Rect(a)
            img.drawRect(re)
            img.finish(width=1, color=blue)
            img.commit(overlay=True)  # store /Contents of out page

        for i in tree_x:
            full_tree_x.add(i)

        full_tree_x.split_overlaps()

        for i in tree_y:
            full_tree_y.add(i)

        full_tree_y.split_overlaps()

        # If there are two columns, the gutter should be in the middle.
        # Margins are the first and last intervals, the ignored parts
        # are the left and right columns.
        left_margin, _, gutter, _, right_margin = \
            map(get_interval_width, list(sorted(full_tree_x)))

        # For top and bottom margins, we only know they are the first and
        # last elements in the list
        full_tree_y_list = list(sorted(full_tree_y))
        top_margin, bottom_margin = \
            map(
                get_interval_width,
                full_tree_y_list[::len(full_tree_y_list) - 1]
            )

        assert gutter > config["margins"].get("min_gutter", 0.2)
        assert left_margin > config["margins"].get("min_lr_margin", 0.625)
        assert right_margin > config["margins"].get("min_lr_margin", 0.625)
        assert top_margin > config["margins"].get("min_top_margin", 0.75)
        assert bottom_margin > config["margins"].get("min_bottom_margin", 1)

    # save output file
    pdf_document.save("layout.pdf",
                      garbage=4, deflate=True, clean=True)
