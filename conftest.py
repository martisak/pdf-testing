import pytest
"""
Configuration for PDF test cases module.
"""


def pytest_addoption(parser):
    """
    We add the --pdf option to be able to specify the PDF filename.
    """
    parser.addoption(
        "--pdf", action="store", default=None, help="filename"
    )


@pytest.fixture(scope="session")
def pdf(request):
    """
    Make sure that the PDF filename is available.
    """

    return request.config.getoption("--pdf")
